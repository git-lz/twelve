package controller_config

import (
	"time"

	"gitee.com/git-lz/twelve/logic/task_manager/crud"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"gitee.com/git-lz/twelve/common/consts"
	"gitee.com/git-lz/twelve/common/dto/dto_task"
	"gitee.com/git-lz/twelve/common/dto/response"
	"gitee.com/git-lz/twelve/common/merrors"
	"gitee.com/git-lz/twelve/common/xtrace"
	"gitee.com/git-lz/twelve/common/zaplog"
)

type TaskController struct {
}

func NewTaskController() *TaskController {
	return &TaskController{}
}

func (tc *TaskController) CreateTask(ctx *gin.Context) {
	start := time.Now()

	c := xtrace.ExtractTrueCtx(ctx)

	resp := &response.Response{}
	defer func() {
		response.EchoResponse(ctx, resp)
	}()

	var req = &dto_task.CreateTaskReq{}
	if err := ctx.Bind(&req); err != nil {
		zaplog.Errorf(c, consts.DLTagRequestBindError, start, zap.Error(err))
		resp.WithMsg(merrors.ErrnoRequestBindFailed, err.Error())
		return
	}

	resp = crud.NewTaskManager().Create(c, req)
	return
}

func (tc *TaskController) GetAllTasks(ctx *gin.Context) {
	start := time.Now()

	c := xtrace.ExtractTrueCtx(ctx)

	resp := &response.Response{}
	defer func() {
		response.EchoResponse(ctx, resp)
	}()

	var req = &dto_task.CreateTaskReq{}
	if err := ctx.Bind(&req); err != nil {
		zaplog.Errorf(c, consts.DLTagRequestBindError, start, zap.Error(err))
		resp.WithMsg(merrors.ErrnoRequestBindFailed, err.Error())
		return
	}

	resp = crud.NewTaskManager().GetAllTasks(c)
	return
}
