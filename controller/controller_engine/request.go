package controller_engine

import (
	"fmt"

	"gitee.com/git-lz/twelve/common/dto/response"
	"gitee.com/git-lz/twelve/common/merrors"
	"gitee.com/git-lz/twelve/common/xtrace"
	"gitee.com/git-lz/twelve/logic/control_plane"
	"github.com/gin-gonic/gin"
)

type RequestController struct{}

func NewRequestController() *RequestController {
	return &RequestController{}
}

func (r *RequestController) DealRequest(ctx *gin.Context) {
	c := xtrace.ExtractTrueCtx(ctx)

	resp := &response.Response{}
	defer func() {
		response.EchoResponse(ctx, resp)
	}()

	path := ctx.Param("path")
	if path == "" {
		resp.WithMsg(merrors.ErrnoDataFormatFailed, fmt.Sprintf("The url=(%s) is not support!", ctx.FullPath()))
		return
	}

	resp = control_plane.New().Entry(c, ctx.Request, path)
}
