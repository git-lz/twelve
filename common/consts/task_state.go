package consts

const (
	TaskStateToAudit   = 0
	TaskStateAuditPass = 1
	TaskStateReject    = 2
)
