package consts

const (
	TimeFormatMinutes3 = "2006-01-02 15:04:05.000"
	TimeFormatMinutes0 = "2006-01-02 15:04:05"
	TimeFormatDay      = "20060102"

	TimeFormatDayWithSplit = "2006-01-02"
)
