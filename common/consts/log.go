package consts

const (
	DLTagSuccess = "success"
	DLTagCost    = "cost"

	DLTagRedisInfo  = "_redis_info"
	DLTagRedisError = "_redis_info_error"

	DLTagMysqlError          = "_mysql_error"
	DLTagMysqlConnectRefused = "_mysql_connect_refused"
	DLTagMysqlConnectTimeout = "_mysql_connect_timeout"
	DLTagMysqlSlowQuery      = "_mysql_slow_query"

	DLTagJsonUnmarshalFailed = "_json_unmarshal_failed"
	DLTagMarshalFailed       = "_json_marshal_failed"
	DLTagFormatConvertFailed = "_format_convert_failed"

	DLTagGetHttpClientFailed = "_get_http_client_failed"
	DLTagGetGrpcClientFailed = "_get_grpc_client_failed"

	DLTagDataNotSupport = "_data_not_support"

	DLTagPanicError = "_panic_error"
)

const (
	CtxKey   = "ctx_key"
	TraceKey = "trace_key"

	TraceIdKey = "trace_id"

	HeaderRidKey = "com-header-rid"

	SyncContextKey = "sync_context"
)

// http 请求
const (
	DLTagHttpSuccess             = "_http_success"
	DLTagHttpFailed              = "_http_failed"
	DLTagRequestBindError        = "_http_error_request_bind"
	DLTagHttpRespUnmarshalFailed = "_http_error_resp_unmarshal"

	DLTagRequestParamError = " _http_error_request_params"

	DLTagLimiterWaitError = " _limiter_wait_error"
)

// task
const (
	DLTagTaskPrepareFailed    = "_task_prepare_failed"
	DLTagTaskNotFind          = "_task_not_find"
	DLTagTaskContextFailed    = "_task_context_failed"
	DLTagTaskExecuteFailed    = "_task_execute_failed"
	DLTagConditionCheckFailed = "_condition_check_failed"
	DLTagActionExecuteFailed  = "_action_execute_failed"
)
