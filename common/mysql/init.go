package mysql

import (
	"fmt"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"

	"gitee.com/git-lz/twelve/common/config"
	"gitee.com/git-lz/twelve/common/zaplog"
)

var DB = new(gorm.DB)

func Init() func() {
	var err error
	DB, err = gorm.Open(mysql.Open(config.Viper.GetString("mysql.dsn")), &gorm.Config{
		Logger: NewLogger(zaplog.MysqlLogger),
	})
	if err != nil {
		panic(fmt.Errorf("mysql err is (%w)", err))
	}

	mysqlDb, err := DB.DB()
	if err != nil {
		panic(fmt.Errorf("mysql err is (%w)", err))
	}
	mysqlDb.SetConnMaxLifetime(time.Second * config.Viper.GetDuration("mysql.max_life_time"))
	mysqlDb.SetMaxOpenConns(config.Viper.GetInt("mysql.max_open_conns"))
	mysqlDb.SetMaxIdleConns(config.Viper.GetInt("mysql.max_idle_conns"))

	cancelFunc := func() {
		if err := mysqlDb.Close(); err != nil {
			fmt.Println("mysql close err is: ", err)
		} else {
			fmt.Println("mysql close success")
		}
	}

	fmt.Println("mysql init success!")
	return cancelFunc
}

// InitTest 在当前package下，最先运行的一个函数，常用于初始化
func InitTest() sqlmock.Sqlmock {
	// 把匹配器设置成相等匹配器，不设置默认使用正则匹配
	db, mock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}

	_DB, err := gorm.Open(mysql.New(mysql.Config{
		Conn:                      db,
		SkipInitializeWithVersion: true,
	}), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	DB = _DB.Debug()

	return mock
}
