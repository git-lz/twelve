package mysql

import (
	"database/sql"
	"fmt"
	"reflect"
	"testing"

	"bou.ke/monkey"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"

	"gitee.com/git-lz/twelve/common/config"
)

func init() {
	config.Viper.AddConfigPath("../../conf/twelve_config/")
	config.Init("")
}

func TestInit(t *testing.T) {
	t.Run("init success", func(t *testing.T) {
		cancel := Init()
		assert.Equal(t, DB.Name(), "mysql")
		cancel()
	})

	t.Run("gorm open error", func(t *testing.T) {
		monkey.Patch(gorm.Open, func(dialector gorm.Dialector, opts ...gorm.Option) (db *gorm.DB, err error) {
			return nil, errors.New("mock")
		})
		defer monkey.Unpatch(gorm.Open)

		defer func() {
			e := recover()
			assert.Contains(t, fmt.Sprintf("%v", e), "mysql err is")
		}()
		Init()
	})

	t.Run("get sql.DB error", func(t *testing.T) {
		monkey.PatchInstanceMethod(reflect.TypeOf(DB), "DB", func(_ *gorm.DB) (*sql.DB, error) {
			return nil, errors.New("mock")
		})
		defer monkey.UnpatchInstanceMethod(reflect.TypeOf(DB), "DB")

		defer func() {
			e := recover()
			assert.Contains(t, fmt.Sprintf("%v", e), "mysql err is")
		}()
		Init()
	})

	t.Run("db close error", func(t *testing.T) {
		cancel := Init()
		assert.Equal(t, DB.Name(), "mysql")

		db := new(sql.DB)
		monkey.PatchInstanceMethod(reflect.TypeOf(db), "Close", func(_ *sql.DB) error {
			return errors.New("mock")
		})
		defer monkey.UnpatchInstanceMethod(reflect.TypeOf(db), "Close")

		cancel()
	})
}

func TestInitTest(t *testing.T) {
	t.Run("init success", func(t *testing.T) {
		mock := InitTest()
		assert.NotNil(t, mock)
	})

	t.Run("sqlmock.New error", func(t *testing.T) {
		monkey.Patch(gorm.Open, func(dialector gorm.Dialector, opts ...gorm.Option) (db *gorm.DB, err error) {
			return nil, errors.New("mock")
		})
		defer monkey.Unpatch(gorm.Open)

		defer func() {
			e := recover()
			assert.Contains(t, fmt.Sprintf("%v", e), "mock")
		}()
		InitTest()
	})
}
