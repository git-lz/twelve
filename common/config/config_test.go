package config

import (
	"fmt"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func init() {
}

func Test_getViper(t *testing.T) {
	t.Run("twelve_config not find", func(t *testing.T) {
		defer func() {
			e := recover()
			assert.Contains(t, fmt.Sprintf("%v", e), "Config File \"app\" Not Found in \"[]\"")
		}()
		Init("")
	})

	t.Run("init success", func(t *testing.T) {
		once = &sync.Once{}
		Viper.AddConfigPath("../../conf/twelve_config/")
		Init("")
	})

	t.Run("init success", func(t *testing.T) {
		once = &sync.Once{}
		Viper.AddConfigPath("../../conf/twelve_config/")
		defer func() {
			e := recover()
			assert.Contains(t, fmt.Sprintf("%v", e), "Config File \"mock\" Not Found")
		}()

		mockConfigs := append(Viper.GetStringSlice("include.configs"), "mock")
		Viper.Set("include.configs", mockConfigs)
		Init("")
	})
}
