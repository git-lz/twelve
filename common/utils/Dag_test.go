package utils

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBFS(t *testing.T) {
	type args struct {
		headNodeId string
		edges      map[string][]string
		visit      func(node string) error
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "test1",
			args: args{
				headNodeId: "1",
				edges: map[string][]string{
					"1": []string{"2"},
					"2": []string{"3"},
					"3": []string{"4"},
				},
				visit: func(node string) error {
					fmt.Println("node: ", node)
					return nil
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := BFS(tt.args.headNodeId, tt.args.edges, tt.args.visit)
			assert.Nil(t, err)
		})
	}
}
