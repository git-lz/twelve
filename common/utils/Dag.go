package utils

func BFS(headNodeId string, edges map[string][]string, visit func(node string) error) error {
	queue := []string{headNodeId}
	visited := make(map[string]bool)

	for len(queue) > 0 {
		node := queue[0]
		queue = queue[1:]

		if visited[node] {
			continue
		}

		err := visit(node)
		if err != nil {
			return err
		}

		visited[node] = true
		for k, v := range edges {
			if k == node {
				queue = append(queue, v...)
			}
		}
	}

	return nil
}
