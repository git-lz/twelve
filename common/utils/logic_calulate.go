package utils

// IsInArray 判断元素是否在数组中
func IsInArray[T comparable](target T, arr []T) bool {
	for _, item := range arr {
		if item == target {
			return true
		}
	}

	return false
}
