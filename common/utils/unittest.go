package utils

/**
 * @Description
 * @Author 007lz
 * @Date 2024/7/10 下午7:35
 **/
import (
	"context"
	"io"
	"log"
	"os"

	"gitee.com/git-lz/twelve/common/mjson"
	"gitee.com/git-lz/twelve/model/task"
)

func GetTestTasks() ([]task.Task, error) {
	jsonFile, err := os.Open("../../docs/tasks_for_unittest.json")
	if err != nil {
		log.Fatal("Error opening JSON file:", err)
	}
	defer jsonFile.Close()

	// 读取 JSON 文件的内容
	byteValue, err := io.ReadAll(jsonFile)
	if err != nil {
		log.Fatal("Error reading JSON file:", err)
	}

	var tasks []task.Task
	if err := mjson.JsonIter.Unmarshal(byteValue, &tasks); err != nil {
		return nil, err
	}

	return tasks, nil
}

func GetDefaultContext() context.Context {
	return context.Background()
}
