package merrors

import "errors"

const (
	ErrMsgSuccess = "success"

	ErrMsgRequestBindFailed  = "request_bind_failed"
	ErrMsgMysqlFailed        = "mysql_failed"
	ErrMsgDataFormatFailed   = "data_format_failed"
	ErrMsgRequestParamsError = "request params error"
	ErrMsgTriggerFailed      = "trigger_failed"
	ErrMsgCaStoreFailed      = "ca_store_failed"

	ErrMsgRecordDuplicateEntry = "Error 1062: Duplicate entry"

	ErrMsgProduceFailed = "Msg Produce failed"

	ErrMsgMysqlDuplicate = "Error 1062 (23000): Duplicate entry"

	ErrMsgMysqlConvertNullToFloat64 = "converting NULL to float64 is unsupported"

	ErrMsgGetConfigFailed         = "Get Config Failed"
	ErrMsgCheckConditionFailed    = "Check Failed"
	ErrMsgGetUserFunnelDataFailed = "Get User Funnel Data failed"

	ErrMsgRedisFailed = "Redis Error"

	ErrMsgInnerErr = "inner error"

	ErrMsgDataIsEmpty = "data is empty"

	ErrMsgExternalErr = "External error"

	ErrMsgDataNotSupport = "data not support"
)

var ErrnoMsgMap = map[int]string{
	ErrnoSuccess:            ErrMsgSuccess,
	ErrnoRequestBindFailed:  ErrMsgRequestBindFailed,
	ErrnoMysqlFailed:        ErrMsgMysqlFailed,
	ErrnoDataFormatFailed:   ErrMsgDataFormatFailed,
	ErrnoRequestParamsError: ErrMsgRequestParamsError,
	ErrnoTriggerFailed:      ErrMsgTriggerFailed,
	ErrnoProduceFailed:      ErrMsgProduceFailed,
	ErrnoCaStoreFailed:      ErrMsgCaStoreFailed,
	ErrnoGetConfigFailed:    ErrMsgGetConfigFailed,

	ErrnoCheckConditionFailed: ErrMsgCheckConditionFailed,
	ErrnoRedisFailed:          ErrMsgRedisFailed,
	ErrnoInnerErr:             ErrMsgInnerErr,
	ErrnoDataIsEmpty:          ErrMsgDataIsEmpty,
	ErrnoExternalErr:          ErrMsgExternalErr,
	ErrnoDataNotSupport:       ErrMsgDataNotSupport,
}

var (
	ErrTemplateNotReviewPass = errors.New("template not review pass")
	ErrTplStatusNotToReview  = errors.New("the state of template is not to review")
	ErrMessageDuplicated     = errors.New("message duplicated")
)
