package merrors

const (
	ErrnoSuccess = 0

	ErrnoRequestBindFailed = 1001 + iota
	ErrnoMysqlFailed
	ErrnoDataFormatFailed
	ErrnoRequestParamsError
	ErrnoTriggerFailed
	ErrnoProduceFailed
	ErrnoCaStoreFailed
	ErrnoGetConfigFailed
	ErrnoCheckConditionFailed
	ErrnoRedisFailed
	ErrnoInnerErr
	ErrnoDataIsEmpty
	ErrnoExternalErr
	ErrnoDataNotSupport
	ErrnoNodeRangeFailed
	ErrnoTaskExecuteFailed
)
