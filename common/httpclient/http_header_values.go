package httpclient

// User-agent
const (
	// HttpUserAgentChromePc chrome pc
	HttpUserAgentChromePc = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11"

	// HttpUserAgentChromeMobile chrome mobile
	HttpUserAgentChromeMobile = "Mozilla/5.0 (Linux; U; Android 2.2.1; zh-cn; HTC_Wildfire_A3333 Build/FRG83D) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"

	// HttpUserAgentFirefoxPc Firefox pc
	HttpUserAgentFirefoxPc = "Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1"

	// HttpUserAgentFirefoxMoblie Firefox Mobil
	HttpUserAgentFirefoxMoblie = "Mozilla/5.0 (Androdi; Linux armv7l; rv:5.0) Gecko/ Firefox/5.0 fennec/5.0"

	// HttpUserAgentIe11 IE11
	HttpUserAgentIe11 = "Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv 11.0) like Gecko"
)

// Content-Type
const (
	HttpContentTypeFromData = "application/x-www-form-urlencoded"
	HttpContentTypeText     = "text/plain"
	HttpContentTypeJson     = "application/json"
	HttpContentTypeXml      = "application/xml"
)
