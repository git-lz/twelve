package httpclient

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInit(t *testing.T) {
	tests := []struct {
		name string
	}{
		{
			name: "case1",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Init()

			clientMap := ClientMap
			fmt.Printf("ClientMap is: %+v \n", clientMap)

			client, err := GetClient(context.Background(), "twelve_config")
			assert.Nil(t, err)
			fmt.Printf("client is: %+v \n", client)
		})
	}
}
