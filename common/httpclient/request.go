package httpclient

import (
	"sync"
)

/**
 * @Description
 * @Author 007lz
 * @Date 2024/7/12 下午5:06
 **/

type Request struct {
	Header *sync.Map
	Params map[string]string
	Body   any
}
