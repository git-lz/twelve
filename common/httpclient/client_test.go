package httpclient

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"reflect"
	"testing"
	"time"

	"gitee.com/git-lz/twelve/common/xtrace"
	"github.com/magiconair/properties/assert"
)

func TestClient_PostJson(t *testing.T) {
	type fields struct {
		timeOut       time.Duration
		header        map[string]string
		cookies       []*http.Cookie
		client        *http.Client
		_header       map[string]string
		_cookies      []*http.Cookie
		buildResponse BuildResponse
	}
	type args struct {
		url   string
		value url.Values
	}

	urlV := url.Values{}
	urlV.Set("mobile", "18289454846")
	urlV.Set("ver_code", "123456")

	tests := []struct {
		name   string
		fields fields
		args   args
		want   IResponse
	}{
		{
			name: "case1",
			fields: fields{
				timeOut: time.Second * 5,
			},
			args: args{
				url:   "https://demo-api.apipost.cn/api/demo/login",
				value: urlV,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			builder := NewClientBuilder()
			builder.timeOut = tt.fields.timeOut
			builder.BuildResponse(EasyBuildResponse)

			client, err := builder.Build()
			assert.Equal(t, err, nil)

			got := client.PostForm(xtrace.NewCtxWithSpan(context.Background(), ""), tt.args.url, tt.args.value)
			if !reflect.DeepEqual(got.Error(), nil) {
				t.Errorf("PostJson() = %v, want %v", got, tt.want)
			}
			fmt.Println("got is: ", string(got.Content()))
		})
	}
}

func TestClient_Get(t *testing.T) {
	type fields struct {
		timeOut       time.Duration
		header        map[string]string
		cookies       []*http.Cookie
		client        *http.Client
		_header       map[string]string
		_cookies      []*http.Cookie
		buildResponse BuildResponse
	}
	type args struct {
		url string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   IResponse
	}{
		{
			name: "case1",
			fields: fields{
				timeOut: time.Second * 5,
			},
			args: args{
				url: "https://demo-api.apipost.cn/api/demo/news_details?id=20&status=1",
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			builder := NewClientBuilder()
			builder.timeOut = tt.fields.timeOut
			builder.BuildResponse(EasyBuildResponse)

			client, _ := builder.Build()
			got := client.Get(xtrace.NewCtxWithSpan(context.Background(), ""), tt.args.url)
			fmt.Println("got is: ", string(got.Content()))
			assert.Equal(t, got.Error(), nil)
		})
	}
}
