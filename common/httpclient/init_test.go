package httpclient

import (
	"gitee.com/git-lz/twelve/common/config"
)

func init() {
	config.Viper.AddConfigPath("../../conf/engine")
	config.Init("")
}
