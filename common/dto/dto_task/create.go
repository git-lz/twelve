package dto_task

import (
	"gitee.com/git-lz/twelve/model/task"
)

// CreateTaskReq 创建任务请求
type CreateTaskReq struct {
	task.Task
}
