package twelve_config

/**
 * @Description 任务配置模块请求api定义
 * @Author 007lz
 * @Date 2024/7/9 下午5:29
 **/
import (
	"context"
	"sync"
	"time"

	"gitee.com/git-lz/twelve/common/config"
	"gitee.com/git-lz/twelve/common/consts"
	"gitee.com/git-lz/twelve/common/zaplog"
	"go.uber.org/zap"
)

var api *ApiInfo
var once = sync.Once{}

func Init() {
	once.Do(func() {
		api = NewApi()
	})
}

type ApiInfo struct {
	Name           string `json:"name"`
	Host           string `json:"host"`
	GetAllTasksUrl string `json:"getAllTasksUrl"`
}

func NewApi() *ApiInfo {
	var apiInfo *ApiInfo
	if err := config.Viper.UnmarshalKey("client.twelve_config", &apiInfo); err != nil {
		panic(err)
	}

	return apiInfo
}

func GetApi(ctx context.Context) *ApiInfo {
	start := time.Now()
	zaplog.Info(ctx, consts.DLTagSuccess, start, zap.Any("api", api))
	return api
}
