package twelve_config

/**
 * @Description 单测
 * @Author 007lz
 * @Date 2024/7/9 下午7:42
 **/
import (
	"context"
	"fmt"
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"gitee.com/git-lz/twelve/common/config"
	"gitee.com/git-lz/twelve/common/httpclient"
	"gitee.com/git-lz/twelve/common/zaplog"
)

func init() {
	config.Init("../../../conf/engine")
	zaplog.Init()
	httpclient.Init()
}

func TestApiInfo_GetAllTasks(t *testing.T) {
	Convey("case1", t, func() {
		a := NewApi()
		got, err := a.GetAllTasks(context.Background())
		if err != nil {
			t.Errorf("GetAllTasks() error = %v, wantErr %v", err, nil)
			return
		}

		fmt.Println("got is: ", got)
	})
}
