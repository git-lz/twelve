package handlers

/**
 * @Description 对外请求方法初始化
 * @Author 007lz
 * @Date 2024/7/9 下午5:43
 **/
import (
	"gitee.com/git-lz/twelve/common/handlers/twelve_config"
)

func EngineHandlersInit() {
	twelve_config.Init()
}
