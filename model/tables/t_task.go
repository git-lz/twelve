package tables

import "time"

type Task struct {
	ID                uint64    `gorm:"primaryKey" json:"id"`                                    // primary id
	TriggerType       string    `gorm:"column:trigger_type" json:"trigger_type"`                 // trigger类型
	TriggerKey        string    `gorm:"column:trigger_key" json:"trigger_key"`                   // trigger唯一键
	BodyPojoClassName string    `gorm:"column:body_pojo_class_name" json:"body_pojo_class_name"` // body pojo类名
	HeadNode          uint64    `gorm:"column:head_node" json:"head_node"`                       // head node
	Version           int8      `gorm:"column:version" json:"version"`                           // 版本号
	Response          string    `gorm:"response" json:"response"`                                // 响应
	State             int8      `gorm:"state" json:"state"`                                      // 状态, ToAudit - 0, AuditPass - 1, AuditReject - 2
	Deleted           int8      `gorm:"column:deleted" json:"deleted"`                           // 是否软删除 0-正常未被软删除 1-已被软删除
	CreateTime        time.Time `json:"create_time" gorm:"autoCreateTime"`
	UpdateTime        time.Time `json:"update_time" gorm:"autoUpdateTime"`
}

const TableNameTask = "t_task"

func (b *Task) TableName() string {
	return TableNameTask
}

func NewTask() *Task {
	return &Task{}
}
