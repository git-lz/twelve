package tables

import "time"

type DagEdge struct {
	ID         uint64    `gorm:"primaryKey" json:"id"`                    // primary id
	TaskId     uint64    `gorm:"column:task_id" json:"task_id"`           // 任务id
	FromNodeId uint64    `gorm:"column:from_node_id" json:"from_node_id"` // 起始node id
	ToNodeIds  string    `gorm:"column:to_node_ids" json:"to_node_ids"`   // 去向node ids,以英文逗号分隔的字符串
	Deleted    int8      `gorm:"column:deleted" json:"deleted"`           // 是否软删除 0-正常未被软删除 1-已被软删除
	CreateTime time.Time `json:"create_time" gorm:"autoCreateTime"`
	UpdateTime time.Time `json:"update_time" gorm:"autoUpdateTime"`
}

const TableNameDagEdge = "t_dag_edge"

func (b *DagEdge) TableName() string {
	return TableNameDagEdge
}

func NewDagEdge() *DagEdge {
	return &DagEdge{}
}
