package tables

import (
	"encoding/json"
	"time"
)

type Action struct {
	ID          uint64          `gorm:"primaryKey" json:"id"`                  // primary id
	Type        string          `gorm:"column:type" json:"type"`               // condition类型
	Config      json.RawMessage `gorm:"column:config" json:"config"`           // condition表达式
	Description json.RawMessage `gorm:"column:description" json:"description"` // condition描述
	Deleted     int8            `gorm:"column:deleted" json:"deleted"`         // 是否软删除 0-正常未被软删除 1-已被软删除
	CreateTime  time.Time       `json:"create_time" gorm:"autoCreateTime"`
	UpdateTime  time.Time       `json:"update_time" gorm:"autoUpdateTime"`
}

const TableNameAction = "t_action"

func (b *Action) TableName() string {
	return TableNameAction
}

func NewAction() *Action {
	return &Action{}
}
