package tables

import (
	"encoding/json"
	"time"
)

type Node struct {
	ID         uint64          `gorm:"primaryKey" json:"id"`            // primary id
	Key        string          `gorm:"column:key" json:"key"`           // node唯一键
	IsAsync    bool            `gorm:"column:is_async" json:"is_async"` // 是否异步
	Elements   json.RawMessage `gorm:"column:elements" json:"elements"` // 元素
	Deleted    int8            `gorm:"column:deleted" json:"deleted"`   // 是否软删除 0-正常未被软删除 1-已被软删除
	CreateTime time.Time       `json:"create_time" gorm:"autoCreateTime"`
	UpdateTime time.Time       `json:"update_time" gorm:"autoUpdateTime"`
}

const TableNameNode = "t_node"

func (b *Node) TableName() string {
	return TableNameNode
}

func NewNode() *Node {
	return &Node{}
}
