package tables

import (
	"encoding/json"
	"time"
)

type Condition struct {
	ID         uint64          `gorm:"primaryKey" json:"id"`                            // primary id
	Type       string          `gorm:"column:type" json:"type"`                         // condition类型
	Name       string          `gorm:"column:name" json:"name"`                         // condition名称
	Expression json.RawMessage `gorm:"column:expression;default:' '" json:"expression"` // condition表达式
	Deleted    int8            `gorm:"column:deleted" json:"deleted"`                   // 是否软删除 0-正常未被软删除 1-已被软删除
	CreateTime time.Time       `json:"create_time" gorm:"autoCreateTime"`
	UpdateTime time.Time       `json:"update_time" gorm:"autoUpdateTime"`
}

const TableNameCondition = "t_condition"

func (b *Condition) TableName() string {
	return TableNameCondition
}

func NewCondition() *Condition {
	return &Condition{}
}
