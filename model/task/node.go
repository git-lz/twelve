package task

// NodeInfo 节点信息
type NodeInfo struct {
	Key      string        `json:"key"`
	Async    bool          `json:"async"`
	Elements []NodeElement `json:"elements"`
}

// NodeElement 节点元素
type NodeElement struct {
	ConditionInfo *ConditionInfo `json:"condition_info"`
	ActionInfo    *ActionInfo    `json:"action_info"`
}

type NodeElementId struct {
	ActionId    uint64
	ConditionId uint64
}
