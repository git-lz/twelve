package task

/**
 * @Description
 * @Author 007lz
 * @Date 2024/7/12 下午3:31
 **/

// ExecActionParams 执行action所需的参数
type ExecActionParams struct {
	NodeId      string
	ActionInfo  *ActionInfo
	TaskContext *TaskContext
}
