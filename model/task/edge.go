package task

type EdgeInfo struct {
	FromToNodeIds map[string][]string `json:"from_to_node_ids"`
}
