package task

import (
	"context"
	"net/http"

	"gitee.com/git-lz/twelve/logic/node"
)

// TaskContext task流转数据
// NodeMap node的集合，map[nodeId]INode，具体的node节点会实现INode中的方法
type TaskContext struct {
	Request *http.Request
	Path    string
	NodeMap map[string]node.INode
}

func NewTaskContext(nodeMap map[string]node.INode) *TaskContext {
	return &TaskContext{NodeMap: nodeMap}
}

func NewDefaultTaskContext() *TaskContext {
	return &TaskContext{}
}

func (t *TaskContext) SetNodeMap(nodeMap map[string]node.INode) *TaskContext {
	t.NodeMap = nodeMap
	return t
}

func (t *TaskContext) GetResponse(ctx context.Context) (interface{}, error) {
	respMap := make(map[string]interface{})

	for nodeId, node := range t.NodeMap {
		respMap[nodeId] = node.GetResponse(ctx)
	}

	return respMap, nil
}
