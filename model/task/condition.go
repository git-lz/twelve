package task

import "gitee.com/git-lz/twelve/model/tables"

func NewConditionInfo() *ConditionInfo {
	return &ConditionInfo{}
}

// ConditionInfo 条件信息
type ConditionInfo struct {
	Type       string `json:"type"`
	Name       string `json:"name"`
	Expression string `json:"expression"`
}

func (c *ConditionInfo) ToDaoCondition() *tables.Condition {
	expression := c.Expression
	if expression == "" {
		expression = " "
	}

	return &tables.Condition{
		Type:       c.Type,
		Name:       c.Name,
		Expression: []byte(c.Expression),
	}
}
