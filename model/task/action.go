package task

import (
	"encoding/json"

	"gitee.com/git-lz/twelve/model/tables"
)

func NewAction() *ActionInfo {
	return &ActionInfo{}
}

type ActionInfo struct {
	Type        string `json:"type"`
	Config      any    `json:"config"`
	Description any    `json:"description"`
}

// ActionConfig action common config
type ActionConfig struct {
	UniqueKey               string         `json:"unique_key"`
	Version                 string         `json:"version"`
	Description             map[string]any `json:"description"`
	RespFormat              string         `json:"resp_format"`
	ResponseClassName       string         `json:"response_class_name"`
	SetToTransferDataMethod string         `json:"set_to_transfer_data_method"`
}

// HttpActionConfig http action twelve_config
type HttpActionConfig struct {
	ActionConfig

	ContentType      string `json:"content_type"`
	HttpVersion      string `json:"http_version"`
	Method           string `json:"method"`
	HostKey          string `json:"host_key"`
	Url              string `json:"url"`
	TimeoutSecond    int    `json:"timeout_second"`
	RetryTimes       int    `json:"retry_times"`
	GetQueryMethod   string `json:"get_query_method"`
	GetBodyMethod    string `json:"get_body_method"`
	GetHeaderMethod  string `json:"get_header_method"`
	GetUrlMethod     string `json:"get_url_method"`
	GetRequestMethod string `json:"get_request_method"`
}

func (a *ActionInfo) ToDaoAction() (*tables.Action, error) {
	config, err := json.Marshal(a.Config)
	if err != nil {
		return nil, err
	}

	description, err := json.Marshal(a.Description)
	if err != nil {
		return nil, err
	}

	return &tables.Action{
		Type:        a.Type,
		Config:      config,
		Description: description,
	}, nil
}
