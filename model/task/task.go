package task

import (
	"gitee.com/git-lz/twelve/model/tables"
)

// Task 任务信息
type Task struct {
	TriggerType       string               `json:"trigger_type"`
	TriggerKey        string               `json:"trigger_key"`
	Version           int8                 `json:"version"`
	BodyPojoClassName string               `json:"body_pojo_class_name"`
	HeadNodeId        string               `json:"head_node_id"`
	Nodes             map[string]*NodeInfo `json:"nodes"`
	Edge              *EdgeInfo            `json:"edge"`
	GetResponseMethod string               `json:"get_response_method"`
}

func NewTask() *Task {
	return &Task{}
}

func (t *Task) ToDaoTask(headNodeId uint64, state int8) *tables.Task {
	return &tables.Task{
		TriggerType:       t.TriggerType,
		TriggerKey:        t.TriggerKey,
		BodyPojoClassName: t.BodyPojoClassName,
		Version:           t.Version,
		Response:          t.GetResponseMethod,
		State:             state,
		HeadNode:          headNodeId,
	}
}
