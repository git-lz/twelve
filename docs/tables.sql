CREATE DATABASE IF NOT EXISTS `twelve`;
USE `twelve`;

CREATE TABLE IF NOT EXISTS `t_action` (
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary id，action_id',
    `type` varchar(16) NOT NULL DEFAULT '' COMMENT 'action类型，目前有HTTP, GRPC, DUBBO2, DUBBO3, Finagle这几种类型',
    `config` mediumtext COMMENT '属性配置，default:{}',
    `description` mediumtext COMMENT '描述信息，default:{}',
    `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否软删除 0-正常未被软删除 1-已被软删除',
    `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY `k_id`(`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='action配置表';

CREATE TABLE IF NOT EXISTS `t_condition` (
                                             `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary id，condition_id',
    `type` varchar(16) NOT NULL DEFAULT '' COMMENT 'condition类型',
    `name` varchar(512) NOT NULL DEFAULT '' COMMENT '名称',
    `expression` mediumtext COMMENT '规则表达式',
    `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否软删除 0-正常未被软删除 1-已被软删除',
    `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY `k_id`(`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='condition配置表';

CREATE TABLE IF NOT EXISTS `t_node` (
                                        `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary id，node_id',
    `name` varchar(256) NOT NULL DEFAULT '' COMMENT 'node名称',
    `is_async` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否是异步调用 0-否，同步调用 1-是，异步调用',
    `type_class_name` varchar(512) NOT NULL DEFAULT '' COMMENT 'node类型类名',
    `elements` mediumtext COMMENT 'condition和action数组，json数组，default:[]',
    `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否软删除 0-正常未被软删除 1-已被软删除',
    `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY `k_id`(`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='node配置表';

CREATE TABLE IF NOT EXISTS `t_task` (
                                        `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary id，task_id',
    `trigger_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'trigger类型，如HTTP',
    `trigger_key` varchar(512) NOT NULL DEFAULT '' COMMENT 'trigger key',
    `body_pojo_class_name` varchar(256) NOT NULL DEFAULT '' COMMENT 'body请求参数的类名',
    `head_node` bigint(20) unsigned NOT NULL DEFAULT 0 COMMENT '指向的node头节点id',
    `version` int(4) NOT NULL DEFAULT 0 COMMENT '版本',
    `response` varchar(256) NOT NULL DEFAULT '' COMMENT '返回值类名',
    `state` int(2) NOT NULL DEFAULT 0 COMMENT '任务状态，0-待审核，1-审核通过，生效中，2-审核拒绝',
    `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否软删除 0-正常未被软删除 1-已被软删除',
    `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY `k_id`(`id`),
    UNIQUE KEY `k_triggerKey_version`(`trigger_key`, `version`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='task配置表';

CREATE TABLE IF NOT EXISTS `t_dag_edge` (
                                            `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary id，edge_id',
    `task_id` bigint(20) unsigned NOT NULL DEFAULT 0 COMMENT '所属的task id',
    `from_node_id` bigint(20) unsigned NOT NULL DEFAULT 0 COMMENT '来源node节点id',
    `to_node_ids` varchar(256) NOT NULL DEFAULT '' COMMENT '指向的node节点id列表，以逗号分隔',
    `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否软删除 0-正常未被软删除 1-已被软删除',
    `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY `k_id`(`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='dag图边定义表';
