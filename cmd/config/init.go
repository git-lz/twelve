package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/gin-gonic/gin"

	"gitee.com/git-lz/twelve/common/config"
	"gitee.com/git-lz/twelve/common/mysql"
	"gitee.com/git-lz/twelve/common/zaplog"
	api "gitee.com/git-lz/twelve/router/config"
)

func Init(path string) {
	gin.SetMode(gin.ReleaseMode)

	config.Init(path)
	zaplog.Init()
	mysqlCancel := mysql.Init()

	go func() {
		defer func() {
			if r := recover(); r != nil {
				fmt.Println("recover error: ", r)
			}
		}()
		api.Init()
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGUSR2, syscall.SIGKILL)

	for {
		select {
		case s := <-c:
			switch s {
			case syscall.SIGTERM, syscall.SIGINT, syscall.SIGUSR2, syscall.SIGKILL:
				fmt.Printf("signal recieve: %s\n", s)
				mysqlCancel()
				return
			case syscall.SIGQUIT:
				fmt.Printf("signal recieve: %s\n", s)
			}
		}
	}
}
