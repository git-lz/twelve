package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitee.com/git-lz/twelve/logic/task_manager/cache"
	"github.com/gin-gonic/gin"

	"gitee.com/git-lz/twelve/common/config"
	"gitee.com/git-lz/twelve/common/handlers"
	"gitee.com/git-lz/twelve/common/httpclient"
	"gitee.com/git-lz/twelve/common/zaplog"
	api "gitee.com/git-lz/twelve/router/engine"
)

func Init(path string) {
	gin.SetMode(gin.ReleaseMode)

	config.Init(path)
	zaplog.Init()
	httpclient.Init()
	handlers.EngineHandlersInit()
	cache.LoadAllTasksFromConfig(context.Background())

	go func() {
		api.Init()
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGUSR2, syscall.SIGKILL)

	for {
		select {
		case s := <-c:
			switch s {
			case syscall.SIGTERM, syscall.SIGINT, syscall.SIGUSR2, syscall.SIGKILL:
				fmt.Printf("signal recieve: %s\n", s)
				return
			case syscall.SIGQUIT:
				fmt.Printf("signal recieve: %s\n", s)
			}
		}
	}
}
