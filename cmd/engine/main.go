package main

import (
	"flag"
	"fmt"
)

var path string

func main() {
	flag.StringVar(&path, "conf", "./conf/engine/", "the twelve_config path")
	flag.Parse()

	fmt.Println("twelve_config path is: ", path)
	Init(path)
}
