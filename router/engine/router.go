package engine

import (
	"fmt"

	"gitee.com/git-lz/twelve/common/config"
	"gitee.com/git-lz/twelve/controller/controller_engine"
	"gitee.com/git-lz/twelve/middleware"
	"github.com/fvbock/endless"
	"github.com/gin-gonic/gin"
)

const (
	pathPrefix = "/twelve"
)

func Init() {
	r := gin.New()

	r.Use(
		middleware.ResponseMid(),
		middleware.SetLoggerMiddleware(),
		middleware.GinRecovery(true),
	)

	groupV1 := r.Group(pathPrefix + "/v1")
	groupV1.POST("/*path", controller_engine.NewRequestController().DealRequest)
	groupV1.GET("/*path", controller_engine.NewRequestController().DealRequest)

	fmt.Println("server in port: ", config.Viper.GetString("server.port"))
	if err := endless.ListenAndServe(config.Viper.GetString("server.port"), r); err != nil {
		fmt.Println("server err is: ", err)
		panic(err)
	}
}
