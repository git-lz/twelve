package api

import (
	"fmt"
	_ "net/http/pprof"

	"gitee.com/git-lz/twelve/controller/controller_config"
	"github.com/fvbock/endless"
	"github.com/gin-gonic/gin"

	"gitee.com/git-lz/twelve/common/config"
	"gitee.com/git-lz/twelve/middleware"
)

const (
	pathPrefix = "/twelve/api"
)

func Init() {
	r := gin.New()

	r.Use(
		middleware.ResponseMid(),
		middleware.SetLoggerMiddleware(),
		middleware.GinRecovery(true),
	)

	groupV1 := r.Group(pathPrefix + "/v1")
	groupV1.POST("/task", controller_config.NewTaskController().CreateTask)
	groupV1.GET("/tasks", controller_config.NewTaskController().GetAllTasks)

	fmt.Println("server in port: ", config.Viper.GetString("server.port"))
	if err := endless.ListenAndServe(config.Viper.GetString("server.port"), r); err != nil {
		fmt.Println("server err is: ", err)
		panic(err)
	}
}
