package dao

import (
	"gitee.com/git-lz/twelve/dao/basedao"
	"gitee.com/git-lz/twelve/model/tables"
)

type taskDao struct {
	*basedao.BaseDao
}

const TableNameTask = "t_task"

func NewTaskDao() *taskDao {
	return &taskDao{
		basedao.NewBaseDao(TableNameTask, tables.Task{}),
	}
}

func (d *taskDao) SetModel(model *tables.Task) *taskDao {
	tx := d
	tx.BaseDao.SetModel(model)
	return tx
}
