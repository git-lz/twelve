package dao

import (
	"gitee.com/git-lz/twelve/dao/basedao"
	"gitee.com/git-lz/twelve/model/tables"
)

type conditionDao struct {
	*basedao.BaseDao
}

const TableNameCondition = "t_condition"

func NewConditionDao() *conditionDao {
	return &conditionDao{
		basedao.NewBaseDao(TableNameCondition, tables.Condition{}),
	}
}

func (d *conditionDao) SetModel(model *tables.Condition) *conditionDao {
	tx := d
	tx.BaseDao.SetModel(model)
	return tx
}
