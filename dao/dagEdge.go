package dao

import (
	"gitee.com/git-lz/twelve/dao/basedao"
	"gitee.com/git-lz/twelve/model/tables"
)

type dagEdgeDao struct {
	*basedao.BaseDao
}

const TableNameDagEdge = "t_dag_edge"

func NewDagEdgeDao() *dagEdgeDao {
	return &dagEdgeDao{
		basedao.NewBaseDao(TableNameDagEdge, tables.DagEdge{}),
	}
}

func (d *dagEdgeDao) SetModel(model *tables.DagEdge) *dagEdgeDao {
	tx := d
	tx.BaseDao.SetModel(model)
	return tx
}
