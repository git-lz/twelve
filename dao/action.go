package dao

import (
	"gitee.com/git-lz/twelve/dao/basedao"
	"gitee.com/git-lz/twelve/model/tables"
)

type actionDao struct {
	*basedao.BaseDao
}

const TableNameAction = "t_action"

func NewActionDao() *actionDao {
	return &actionDao{
		basedao.NewBaseDao(TableNameAction, tables.Action{}),
	}
}

func (d *actionDao) SetModel(model *tables.Action) *actionDao {
	tx := *d
	tx.BaseDao.SetModel(model)
	return &tx
}
