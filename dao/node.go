package dao

import (
	"gitee.com/git-lz/twelve/dao/basedao"
	"gitee.com/git-lz/twelve/model/tables"
)

type nodeDao struct {
	*basedao.BaseDao
}

const TableNameNode = "t_node"

func NewNodeDao() *nodeDao {
	return &nodeDao{
		basedao.NewBaseDao(TableNameNode, tables.Node{}),
	}
}

func (d *nodeDao) SetModel(model *tables.Node) *nodeDao {
	tx := d
	tx.BaseDao.SetModel(model)
	return tx
}
