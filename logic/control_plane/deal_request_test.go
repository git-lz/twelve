package control_plane

/**
 * @Description
 * @Author 007lz
 * @Date 2024/7/10 下午7:44
 **/
import (
	"net/http"
	"strconv"
	"testing"

	"gitee.com/git-lz/twelve/common/config"
	"gitee.com/git-lz/twelve/common/zaplog"
	"gitee.com/git-lz/twelve/logic/task_manager/cache"
	. "github.com/smartystreets/goconvey/convey"

	"gitee.com/git-lz/twelve/common/utils"
)

func init() {
	config.Init("../../conf/engine")
	zaplog.Init()
}

func TestControlPlane_setTask(t *testing.T) {
	Convey("success", t, func() {
		tasks, err := utils.GetTestTasks()
		So(err, ShouldBeNil)
		So(len(tasks), ShouldBeGreaterThan, 1)
		task := tasks[0]

		taskMap := make(map[string]map[string]task.Task)
		taskVersionMap := map[string]task.Task{
			strconv.Itoa(int(task.Version)): task,
		}
		taskMap[task.TriggerKey] = taskVersionMap
		cache.Init(taskMap)

		httpRequest := &http.Request{
			Method: http.MethodGet,
		}
		controlPlane := New(httpRequest, "apihubConfigAllTasks")
		err1 := controlPlane.setTask(utils.GetDefaultContext())
		So(err1, ShouldBeNil)
		So(*controlPlane.Task, ShouldEqual, task)
	})
}
