package control_plane

import (
	"context"
	"errors"
	"fmt"

	"gitee.com/git-lz/twelve/model/task"
)

const defaultVersion = "1"

/** TODO: 添加版本筛选逻辑，目前用默认版本
 * @Description 版本过滤
 * @Author 007lz
 * @Date 2024/7/10 下午5:16
 **/
func (c *ControlPlane) filterByVersion(ctx context.Context, version2Tasks map[string]task.Task) error {
	task, ok := version2Tasks[defaultVersion]
	if !ok {
		return errors.New(fmt.Sprintf("not find the task of default version"))
	}

	c.Task = &task
	return nil
}
