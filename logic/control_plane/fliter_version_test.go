package control_plane

/**
 * @Description
 * @Author 007lz
 * @Date 2024/7/10 下午7:19
 **/
import (
	"testing"

	"gitee.com/git-lz/twelve/model/task"
	. "github.com/smartystreets/goconvey/convey"

	"gitee.com/git-lz/twelve/common/utils"
)

func TestControlPlane_filterByVersion(t *testing.T) {
	Convey("success", t, func() {
		tasks, err := utils.GetTestTasks()
		So(err, ShouldBeNil)

		version2Tasks := map[string]task.Task{
			defaultVersion: tasks[0],
		}

		controlPlane := NewDefault()
		err1 := controlPlane.filterByVersion(utils.GetDefaultContext(), version2Tasks)
		So(err1, ShouldBeNil)
		So(*controlPlane.Task, ShouldEqual, tasks[0])
	})
}
