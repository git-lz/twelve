package condition

/**
 * @Description condition接口
 * @Author 007lz
 * @Date 2024/7/16 下午4:29
 **/
import (
	"context"

	"gitee.com/git-lz/twelve/model/task"
)

// ICondition condition接口
type ICondition interface {
	Check(ctx context.Context, conditionInfo *task.ConditionInfo, taskContext *task.TaskContext) error
}

func CheckCondition(ctx context.Context, conditionInfo *task.ConditionInfo, taskContext *task.TaskContext) error {

	return nil
}
