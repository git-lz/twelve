package condition

import (
	"context"

	"gitee.com/git-lz/twelve/model/task"
)

/**
 * @Description condition判断
 * @Author 007lz
 * @Date 2024/7/12 下午7:29
 **/

// DemoCondition
// condition demo
type DemoCondition struct {
}

func NewCondition() *DemoCondition {
	return &DemoCondition{}
}

func (c *DemoCondition) Check(ctx context.Context, conditionInfo *task.ConditionInfo, taskContext *task.TaskContext) error {
	return nil
}
