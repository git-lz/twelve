package node

/**
 * @Description
 * @Author 007lz
 * @Date 2024/7/11 下午2:32
 **/
import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewINode(t *testing.T) {
	t.Run("case1", func(t *testing.T) {
		ctx := context.Background()
		got, err := NewINode("TaskConfigNode")
		assert.Nil(t, err)
		fmt.Println("got is: ", got.GetResponse(ctx))
		got.SetData(ctx, "test-got")

		got1, err := NewINode("TaskConfigNode")
		assert.Nil(t, err)
		fmt.Println("got is: ", got1.GetResponse(context.Background()))
	})
}
