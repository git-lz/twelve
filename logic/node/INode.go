package node

/**
 * @Description
 * @Author 007lz
 * @Date 2024/7/11 上午10:51
 **/
import (
	"context"
	"errors"
	"fmt"

	"gitee.com/git-lz/twelve/logic/node/task_config"
)

type newInodeFunc func() INode

type INode interface {
	GetResponse(ctx context.Context) any
	SetData(ctx context.Context, data any) error
	GetRequestParams(ctx context.Context) (interface{}, error)
}

var nodekeyToInstanceMap = map[string]newInodeFunc{
	"getAllTasks": func() INode { return task_config.NewTaskConfigNode() },
}

func NewINode(key string) (INode, error) {
	newNodeFunc, ok := nodekeyToInstanceMap[key]
	if !ok {
		return nil, errors.New(fmt.Sprintf("node type=(%v) not support!", key))
	}

	return newNodeFunc(), nil
}
