package task_config

/**
 * @Description
 * @Author 007lz
 * @Date 2024/7/11 下午2:29
 **/
import (
	"context"
	"sync"

	"gitee.com/git-lz/twelve/common/httpclient"
)

type TaskConfigNode struct {
	data interface{}
}

func NewTaskConfigNode() *TaskConfigNode {
	return &TaskConfigNode{}
}

func (t *TaskConfigNode) GetResponse(ctx context.Context) any {
	return t.data
}

func (t *TaskConfigNode) SetData(ctx context.Context, data any) error {
	t.data = data
	return nil
}

func (t *TaskConfigNode) GetRequestParams(ctx context.Context) (interface{}, error) {
	header := &sync.Map{}

	return &httpclient.Request{
		Header: header,
	}, nil
}
