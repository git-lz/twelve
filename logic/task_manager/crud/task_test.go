package crud

import (
	"context"
	"regexp"
	"testing"

	"bou.ke/monkey"
	"gitee.com/git-lz/twelve/common/dto/dto_task"
	"gitee.com/git-lz/twelve/common/merrors"
	"gitee.com/git-lz/twelve/common/mysql"
	"gitee.com/git-lz/twelve/common/utils"
	"gitee.com/git-lz/twelve/dao/basedao"
	"gitee.com/git-lz/twelve/model/task"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
)

func TestTaskManager_Create_Mock(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		patch := monkey.Patch(utils.BFS, func(headNodeId string, fromToNodeIds map[string][]string, f func(node string) error) error {
			return nil
		})
		defer patch.Unpatch()

		sqlMock := mysql.InitTest()
		sqlMock.ExpectBegin()
		insertSql := "INSERT INTO"
		sqlMock.ExpectExec(regexp.QuoteMeta(insertSql)).WillReturnResult(sqlmock.NewResult(1, 1))
		sqlMock.ExpectCommit()

		taskManager := NewTaskManager()
		resp := taskManager.Create(context.Background(), &dto_task.CreateTaskReq{
			Task: task.Task{
				Edge: &task.EdgeInfo{
					FromToNodeIds: map[string][]string{},
				},
			},
		})
		assert.Equal(t, true, resp.IsSucc)
	})

	t.Run("BFS failed", func(t *testing.T) {
		patch := monkey.Patch(utils.BFS, func(headNodeId string, fromToNodeIds map[string][]string, f func(node string) error) error {
			return errors.New("bfs failed")
		})
		defer patch.Unpatch()

		resp := NewTaskManager().Create(context.Background(), &dto_task.CreateTaskReq{
			Task: task.Task{
				Edge: &task.EdgeInfo{
					FromToNodeIds: map[string][]string{},
				},
			},
		})
		assert.Equal(t, false, resp.IsSucc)
		assert.Equal(t, merrors.ErrnoNodeRangeFailed, resp.Errno)
	})

	t.Run("mysql insert failed", func(t *testing.T) {
		patch := monkey.Patch(utils.BFS, func(headNodeId string, fromToNodeIds map[string][]string, f func(node string) error) error {
			return nil
		})
		defer patch.Unpatch()

		sqlMock := mysql.InitTest()
		sqlMock.ExpectBegin()
		insertSql := "INSERT INTO"
		sqlMock.ExpectExec(regexp.QuoteMeta(insertSql)).WillReturnError(errors.New("mysql failed"))
		sqlMock.ExpectCommit()

		resp := NewTaskManager().Create(context.Background(), &dto_task.CreateTaskReq{
			Task: task.Task{
				Edge: &task.EdgeInfo{
					FromToNodeIds: map[string][]string{},
				},
			},
		})
		assert.Equal(t, false, resp.IsSucc)
		assert.Equal(t, merrors.ErrnoMysqlFailed, resp.Errno)
	})
}

// TODO: FIXME: This test is incomplete
func TestTaskManager_GetNodeElements(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		sqlMock := mysql.InitTest()
		sqlMock.ExpectBegin()
		insertSql := "INSERT"
		sqlMock.ExpectExec(insertSql).WillReturnResult(sqlmock.NewResult(1, 1))
		sqlMock.ExpectCommit()

		elements := []task.NodeElement{
			{
				ConditionInfo: &task.ConditionInfo{
					Type: "TestCondition",
				},
				ActionInfo: &task.ActionInfo{
					Type: "TestAction",
				},
			},
		}
		tx := basedao.NewDefaultBaseDao()
		res, err := NewTaskManager().getNodeElements(context.Background(), tx, elements)
		assert.Nil(t, err)
		assert.Equal(t, 1, len(res))
	})
}
