package cache

import (
	"strconv"
	"sync"

	"gitee.com/git-lz/twelve/model/task"
)

var globalTaskMemCache *TaskMemCache

var lock = sync.Mutex{}

type TaskMemCache struct {
	// map[triggerKey][taskVersion]taskInfo，双buffer机制，只读对象
	readTaskMemCache map[string]map[string]task.Task
	// map[triggerKey][taskVersion]taskInfo，双buffer机制，只写对象
	writeTaskMemCache map[string]map[string]task.Task
}

/**
 * @Description 获取全局的内存任务配置
 **/
func GetGlobalTaskMemCache() map[string]map[string]task.Task {
	return globalTaskMemCache.readTaskMemCache
}

/**
 * @Description 初始化任务内存配置
 * @Param taskInfo 任务配置
 **/
func Init(taskInfo map[string]map[string]task.Task) {
	if globalTaskMemCache != nil {
		return
	}

	globalTaskMemCache = new(TaskMemCache)
	globalTaskMemCache.writeTaskMemCache = taskInfo
	globalTaskMemCache.readTaskMemCache = taskInfo
}

/**
 * @Description 添加或者更新内存任务配置
 * @Param taskInfo 任务配置
 **/
func AddOrUpdate(tk task.Task) {
	lock.Lock()
	defer lock.Unlock()

	versionTaskMap, ok := globalTaskMemCache.writeTaskMemCache[tk.TriggerKey]
	if !ok { // If the task is not exits, add it.
		globalTaskMemCache.writeTaskMemCache[tk.TriggerKey] = map[string]task.Task{
			strconv.Itoa(int(tk.Version)): tk,
		}
	} else {
		versionTaskMap[strconv.Itoa(int(tk.Version))] = tk
		globalTaskMemCache.writeTaskMemCache[tk.TriggerKey] = versionTaskMap
	}

	swap()
}

/**
 * @Description 删除内存中的任务配置
 * @Param task 任务配置
 **/
func DeleteTask(task task.Task) {
	lock.Lock()
	defer lock.Unlock()

	versionTaskMap, ok := globalTaskMemCache.writeTaskMemCache[task.TriggerKey]
	if !ok {
		return
	}

	delete(versionTaskMap, strconv.Itoa(int(task.Version)))
	swap()
}

/**
 * @Description 双buffer交换
 **/
func swap() {
	lock.Lock()
	globalTaskMemCache.readTaskMemCache = globalTaskMemCache.writeTaskMemCache
	lock.Unlock()
}
