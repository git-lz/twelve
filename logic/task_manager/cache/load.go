package cache

import (
	"context"
	"strconv"
	"time"

	"gitee.com/git-lz/twelve/common/consts"
	"gitee.com/git-lz/twelve/common/handlers/twelve_config"
	"gitee.com/git-lz/twelve/common/zaplog"
	"gitee.com/git-lz/twelve/model/task"
	"go.uber.org/zap"
)

/**
 * @Description 从config模块获取全量任务配置
 * @Author 007lz
 * @Date 2024/7/9 下午4:41
 **/
func LoadAllTasksFromConfig(ctx context.Context) {
	start := time.Now()

	tasks, err := twelve_config.GetApi(ctx).GetAllTasks(ctx)
	if err != nil {
		zaplog.Errorf(ctx, consts.DLTagHttpFailed, start, zap.Error(err))
		return
	}

	// map[triggerKey]map[version]taskInfo
	taskMap := make(map[string]map[string]task.Task)

	var triggerKeys []string
	for _, task := range tasks {
		triggerKeys = append(triggerKeys, task.TriggerKey)
	}

	var versionsMap = make(map[string]task.Task)
	for _, task := range tasks {
		versionsMap[strconv.Itoa(int(task.Version))] = task
	}

	for _, triggerKey := range triggerKeys {
		for _, task := range versionsMap {
			if triggerKey == task.TriggerKey {
				taskMap[triggerKey] = versionsMap
			}
		}
	}

	Init(taskMap)
}
