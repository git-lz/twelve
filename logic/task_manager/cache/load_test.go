package cache

/**
 * @Description
 * @Author 007lz
 * @Date 2024/7/9 下午7:57
 **/
import (
	c "context"
	"reflect"
	"testing"

	"gitee.com/git-lz/twelve/model/task"
	"github.com/smartystreets/goconvey/convey"

	"bou.ke/monkey"
	"gitee.com/git-lz/twelve/common/config"
	"gitee.com/git-lz/twelve/common/handlers"
	"gitee.com/git-lz/twelve/common/handlers/twelve_config"
	"gitee.com/git-lz/twelve/common/httpclient"
	"gitee.com/git-lz/twelve/common/utils"
	"gitee.com/git-lz/twelve/common/zaplog"
)

func init() {
	config.Init("../../../conf/engine")
	handlers.EngineHandlersInit()
	zaplog.Init()
	httpclient.Init()
}

// TODO: FIXME
func TestLoadAllTasksFromConfig(t *testing.T) {
	tasks, err := utils.GetTestTasks()
	convey.So(err, convey.ShouldBeNil)

	convey.Convey("success", t, func() {
		ctx := utils.GetDefaultContext()
		api := twelve_config.GetApi(ctx)
		patch := monkey.PatchInstanceMethod(reflect.TypeOf(api), "GetAllTasks",
			func(_ *twelve_config.ApiInfo, ctx c.Context) ([]task.Task, error) {
				return tasks, nil
			})
		defer patch.Unpatch()

		LoadAllTasksFromConfig(ctx)

		tasksInMem := GetGlobalTaskMemCache()
		var tasksInMems []task.Task
		for _, tasksInMemVersion := range tasksInMem {
			for _, taskInfo := range tasksInMemVersion {
				tasksInMems = append(tasksInMems, taskInfo)
			}
		}

		convey.So(len(tasksInMems), convey.ShouldEqual, len(tasks))
	})
}
